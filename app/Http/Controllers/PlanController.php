<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use DB;
use App\Plan;
use App\PlanDetails;
use App\TransacPlanDetail;

class PlanController extends Controller
{
    public function __construct()
    {
        // do
    }

    public function addPlans(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|regex:/^[\pL\s\-]+$/u',
            'description' => 'required',
            'status' => 'required|numeric',
            'price' => 'required|numeric',
            'currency' => 'required',
            'expiration_time' => 'required|numeric',
            'publications_for_sale' => 'required|numeric',
            'publications_for_rent' => 'required|numeric'
        ], [
            'name.required' => 'Nombre requerido.',
            'name.regex' => 'Solo letras permitidas para Nombre.',
            'description.required' => 'Descripción requerida.',
            'status.required' => 'Estado requerido.',
            'status.numeric' => 'Estado invalido.',
            'price.required' => 'Precio requerido.',
            'price.numeric' => 'Precio invalido.',
            'currency.required' => 'Moneda requerida.',
            'expiration_time.required' => 'Tiempo de caducación requerido.',
            'expiration_time.numeric' => 'Tiempo de caducación invalido.',
            'publications_for_sale.required' => 'Cantidad de ventas requerido.',
            'publications_for_sale.numeric' => 'Cantidad de ventas invalido.',
            'publications_for_rent.required' => 'Cantidad de alquiler requerido.',
            'publications_for_rent.numeric' => 'Cantidad de alquiler invalido.'
        ]);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 401);
        }

        $body = $request->all();
        
        $plan = new Plan;
        $plan->name = $body['name'];
        $plan->description = $body['description'];
        $plan->status = $body['status'];

        $planSaved = $plan->save();

        if (!$planSaved) {
            return response()->json(['success' => false, 'message' => 'Error al agregar plan.'], 500);
        } else {
            $planDetail = new PlanDetails;

            $planDetail->currency = $body['currency'];
            $planDetail->price = $body['price'];
            $planDetail->expiration_time = $body['expiration_time'];
            $planDetail->publications_for_sale = $body['publications_for_sale'];
            $planDetail->publications_for_rent = $body['publications_for_rent'];

            $planDetailSaved = $planDetail->save();

            if (!$planDetailSaved) {
                return response()->json(['success' => false, 'message' => 'Error al agregar detalles del plan.'], 500);
            } else {

                $transacPlanDetail = new TransacPlanDetail;

                $transacPlanDetail->plan_id = $plan->id;
                $transacPlanDetail->plan_detail_id = $planDetail->id;

                $transacPlanDetailSaved = $transacPlanDetail->save();

                if (!$transacPlanDetailSaved) {
                    return response()->json(['success' => false, 'message' => 'Error al asociar plan con detalles.'], 500);
                } else {
                    return response()->json(['success' => true, 'message' => 'Plan agregado exitosamente.'], 200);
                }
            }
        }
    }

    public function findOnePlan(Request $request)
    {
        $planId = $request->route('id');

        $planFinded = Plan::where('id', $planId)
                        ->with('TransacPlanDetail.PlanDetails')
                        ->take(1)
                        ->get();

        if (!$planFinded->count()) {
            return response()->json(['success' => false, 'message' => 'Plan '.$planId.' no encontrado.'], 404);
        } else {
            return response()->json(['success' => true, 'content' => $planFinded], 200);
        }
    }

    public function findPlans(Request $request)
    {
        $offset = ($request->offset !== null) ? $request->offset : 0;
        $limit = ($request->limit !== null) ? $request->limit : 10;
        $searchType = ($request->searchType !== null) ? $request->searchType : "all";

        $verifyArr = ['limit' => $limit, 'offset' => $offset];


        $validator = Validator::make($verifyArr, [
            'limit' => 'numeric',
            'offset' => 'numeric'
        ], [
            'limit.numeric' => 'Limit debe ser numerico.',
            'offset.numeric' => 'Offset debe ser numerico.'
        ]);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 400);
        }

        if ($request->search === 'activo') {
            $request->search = 1;
        } elseif ($request->search === 'inactivo') {
            $request->search = 0;
        }

        $allPlansFinded = Plan::select([DB::raw("
            SQL_CALC_FOUND_ROWS
                plans.id,
                plans.name,
                plans.description,
                plans.status,
                plan_details.currency,
                plan_details.price,
                plan_details.expiration_time,
                plan_details.publications_for_sale,
                plan_details.publications_for_rent
            ")])
        ->leftJoin('transac_plan_details', 'plans.id', '=', 'transac_plan_details.plan_id')
        ->leftJoin('plan_details', 'transac_plan_details.plan_detail_id', '=', 'plan_details.id')
        ->orWhere('name', 'like', '%' .$request->search. '%')
        ->orWhere('price', 'like', '%' .$request->search. '%')
        ->orWhere('status', 'like', '%' .$request->search. '%')
        ->orderBy('id', $request->order)
        ->take($limit)
        ->skip($offset)
        ->get();

        $countRows = DB::select(DB::raw("SELECT FOUND_ROWS() AS totalCount;"));

        return response()->json(["total" => $countRows[0]->totalCount, "rows" => $allPlansFinded], 200);
    }

    public function deleteOnePlan(Request $request)
    {
        $planId = $request->route('id');

        $planExist = Plan::where('id', '=', $planId)
                        ->first();

        $transacPlanDetailExist = TransacPlanDetail::where('plan_id', '=', $planId)
                                    ->first()->plan_detail_id;

        if ($planExist === null) {
            return response()->json(['success' => false, 'message' => 'Plan '.$planId.' no encontrado.'], 404);
        } else {

            if ($transacPlanDetailExist === null) {
                return response()->json(['success' => false, 'message' => 'Plan detalle '.$transacPlanDetailExist.' no encontrado.'], 404);
            } else {
                $planDetailExist = PlanDetails::where('id', '=', $transacPlanDetailExist)
                                    ->first();

                $planExist->delete();
                $planDetailExist->delete();


                return response()->json(['success' => true, 'message' => 'Plan eliminado exitosamente.'], 200);
            }
        }
    }

    public function updatePlan(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'editName' => 'sometimes|regex:/^[\pL\s\-]+$/u',
            'editDescription' => 'sometimes',
            'editStatus' => 'sometimes|numeric',
            'editPrice' => 'sometimes|numeric',
            'editCurrency' => 'sometimes',
            'editExpirationTime' => 'sometimes|numeric',
            'editPublicationForSale' => 'sometimes|numeric',
            'editPublicationForRent' => 'sometimes|numeric'
        ], [
            'editName.regex' => 'Solo letras permitidas para Nombre.',
            'editStatus.numeric' => 'Estado invalido.',
            'editPrice.numeric' => 'Precio invalido.',
            'editExpirationTime.numeric' => 'Tiempo de caducación invalido.',
            'editPublicationForSale.numeric' => 'Cantidad de ventas invalido.',
            'editPublicationForRent.numeric' => 'Cantidad de alquiler invalido.'
        ]);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 400);
        }

        $planId = $request->route('id');
        $body = $request->all();

        $planFinded = Plan::where('id', '=', $planId)
                        ->first();
        $transacPlanDetailFinded = TransacPlanDetail::where('plan_id', '=', $planId)
                                ->first()->plan_detail_id;
        $planDetailFinded = PlanDetails::where('id', '=', $transacPlanDetailFinded)
                        ->first();

        if ($planFinded === null) {
            return response()->json(['success' => false, 'message' => 'Plan '.$planId.' no encontrado.'], 404);
        } else {
            if ($transacPlanDetailFinded === null) {
                return response()->json(['success' => false, 'message' => 'Plan detalle '.$transacPlanDetailFinded.' no encontrado.'], 404);
            } else {
                if (array_key_exists('editName', $body)) {
                    $planFinded->name = $body['editName'];
                }
                if (array_key_exists('editDescription', $body)) {
                    $planFinded->description = $body['editDescription'];
                }
                if (array_key_exists('editStatus', $body)) {
                    $planFinded->status = $body['editStatus'];
                }
                if (array_key_exists('editPrice', $body)) {
                    $planDetailFinded->price = $body['editPrice'];
                }
                if (array_key_exists('editCurrency', $body)) {
                    $planDetailFinded->currency = $body['editCurrency'];
                }
                if (array_key_exists('editExpirationTime', $body)) {
                    $planDetailFinded->expiration_time = $body['editExpirationTime'];
                }
                if (array_key_exists('editPublicationForSale', $body)) {
                    $planDetailFinded->publications_for_sale = $body['editPublicationForSale'];
                }
                if (array_key_exists('editPublicationForRent', $body)) {
                    $planDetailFinded->publications_for_rent = $body['editPublicationForRent'];
                }

                $planFinded->save();
                $planDetailFinded->save();

                return response()->json(['success' => true, 'message' => 'Plan actualizado exitosamente.'], 200);
            }
        }
    }

}
