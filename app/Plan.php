<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Plan extends Model
{

	protected $fillable = [
		'name',
		'description',
		'status',
		'created'
	];

	protected $guarded = [
		'id'
	];

	protected $table = 'plans';

	public $timestamps = false;

	public function TransacPlanDetail()
	{
		return $this->hasMany(TransacPlanDetail::class, 'plan_id', 'id');
	}
	
}
