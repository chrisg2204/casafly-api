<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\User;
use Validator;
use Mail;

class PassportController extends Controller
{

    public $successStatus = 200;

    public function login(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|email',
            'password' => 'required',
        ], [
            'email.required' => 'Email requerido.',
            'email.email' => 'Email invalido.',
            'password.required' => 'Password requerido.'
        ]);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 400);
        }

        if(Auth::attempt(['email' => request('email'), 'password' => request('password')])) {

            $user = Auth::user();
            $success['token'] =  $user->createToken('MyApp')->accessToken;

            return response()->json(['success' => $success, 'data' => $user], 200);
        } else {
            return response()->json(['data' => 'Usuario o contraseña invalidos.'], 401);
        }
    }

    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'firstname' => 'required|regex:/^[\pL\s\-]+$/u',
            'lastname' => 'required|regex:/^[\pL\s\-]+$/u',
            'email' => 'required|email',
            'phone' => 'required|numeric'
        ], [
            'firstname.required' => 'Nombre requerido.',
            'firstname.regex' => 'Solo letras permitidas para Nombres.',
            'lastname.required' => 'Apellido requerido.',
            'lastname.regex' => 'Solo letras permitidas para Apellidos.',
            'email.required' => 'Email requerido.',
            'email.email' => 'Email invalido.',
            'phone.required' => 'Teléfono requerido.',
            'phone.numeric' => 'Teléfono invalido.'
        ]);

        if ($validator->fails()) {
            return response()->json(['error'=>$validator->errors()], 401);
        }

        $input = $request->all();

        $userExist = User::where('email', '=', $input['email'])->first();

        if ($userExist === null) {
            $passwordBeforeCrypt = str_random(8);
            $input['password'] = bcrypt($passwordBeforeCrypt);

            $user = User::create($input);
            $input['token'] =  $user->createToken('MyApp')->accessToken;
            $input['passwordBeforeCrypt'] = $passwordBeforeCrypt;

            Mail::send('emails.welcome', $input, function($message) use ($request) {
                $message->from('soporte@casafly.com', 'Soporte Casafly');
                $message->subject('Bienvenido a Casafly!');
                $message->to($request['email'], $request['firstname']);
            });

            return response()->json(['success' => true, 'message' => 'Registro exitoso. Su clave fue enviada a su correo.'], 200);
        } else {
            return response()->json(['success' => false, 'message' => 'Email'.' '.$input['email'].' '.'ya existe.'], 400);
        }
    }

    public function getDetails()
    {
        if (Auth::check()) {
            $user = Auth::user();

            return response()->json(['success' => $user], 200);
        }
    }

    public function logout(Request $request)
    {
        if (Auth::check()) {
            $request->user()->token()->delete();

            return response()->json(['success' => true, 'message' => 'Sesión finalizada'], 200);
        }
    }

    public function changePassword(Request $request)
    {
        if (Auth::check()) {
            $validator = Validator::make($request->all(), [
                'newPassword' => 'required',
                'newPasswordConfirm' => 'required|same:newPassword',
            ], [
                'newPassword.required' => 'Contraseña requerida.',
                'newPasswordConfirm.required' => 'Confirme contraseña para continuar.',
                'newPasswordConfirm.same' => 'Contraseñas no coinciden.'
            ]);

            if ($validator->fails()) {
                return response()->json(['error'=>$validator->errors()], 400);
            }

            $input = $request->all();
            $user = Auth::user();

            $user->password = bcrypt($input['newPassword']);
            $user->save();

            $user->token()->revoke();
            $user->createToken('MyApp')->accessToken;

            return response()->json(['success' => true, 'message' => 'Contraseña cambiada exitosamente.'], 200);
        }
    }

}
