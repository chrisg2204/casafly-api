<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Validator;

class UserController extends Controller
{

    public function __construct()
    {
        // Do.
    }

    public function findUsers(Request $request)
    {
        $offset = ($request->offset !== null) ? $request->offset : 0;
        $limit = ($request->limit !== null) ? $request->limit : 10;
        $searchType = ($request->searchType !== null) ? $request->searchType : "all";

        $verifyArr = ['limit' => $limit, 'offset' => $offset];

        $validator = Validator::make($verifyArr, [
            'limit' => 'numeric',
            'offset' => 'numeric'
        ], [
            'limit.numeric' => 'Limit debe ser numerico.',
            'offset.numeric' => 'Offset debe ser numerico.'
        ]);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 400);
        }

        $allUsersFinded = User::take($limit)
                            ->skip($offset)
                            ->orWhere('firstname', 'like', '%' .$request->search. '%')
                            ->orWhere('lastname', 'like', '%' .$request->search. '%')
                            ->orWhere('email', 'like', '%' .$request->search. '%')
                            ->get();

        return response()->json(["total" => $allUsersFinded->count(), "rows" => $allUsersFinded], 200);
    }

    public function updateUser(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'firstname' => 'sometimes|regex:/^[\pL\s\-]+$/u',
            'lastname' => 'sometimes|regex:/^[\pL\s\-]+$/u',
            'nationality' => 'sometimes|regex:/^[\pL\s\-]+$/u',
            'phone' => 'sometimes|numeric',
            'phone_optional' => 'sometimes|numeric'
        ], [
            'firstname.regex' => 'Solo letras permitidas para Nombres.',
            'lastname.regex' => 'Solo letras permitidas para Apellidos.',
            'nationality.regex' => 'Solo letras permitidas para Nacionalidad.',
            'phone.numeric' => 'Teléfono invalido.',
            'phone_optional.numeric' => 'Teléfono invalido.'
        ]);

        if ($validator->fails()) {
            return response()->json(['error'=>$validator->errors()], 400);
        }

        $userId = $request->route("id");
        $body = $request->all();

        $userFinded = User::where("id", "=", $userId)
                        ->first();

        if ($userFinded === null) {
            return response()->json(["success" => false, "message" => "Vendedor ".$userId." no encontrado."], 404);
        } else {
            if (array_key_exists("firstname", $body)) {
                $userFinded->firstname = $body["firstname"];
            }
            if (array_key_exists("lastname", $body)) {
                $userFinded->lastname = $body["lastname"];
            }
            if (array_key_exists("nationality", $body)) {
                $userFinded->nationality = $body["nationality"];
            }
            if (array_key_exists("phone", $body)) {
                $userFinded->phone = $body["phone"];
            }
            if (array_key_exists("phone_optional", $body)) {
                $userFinded->phone_optional = $body["phone_optional"];
            }

            $userSaved = $userFinded->save();

            if (!$userSaved) {
                return response()->json(["success" => false, "message" => "Error al actualizar datos del vendedor."], 500);
            } else {
                return response()->json(["success" => true, "message" => "Vendedor actualizado exitosamente."], 200);
            }
        }
    }

    public function deleteUser(Request $request)
    {
        $userId = $request->route("id");

        $userExist = User::where("id", "=", $userId)
                        ->first();

        if ($userExist === null) {
            return response()->json(["success" => false, "message" => "Vendedor ".$userId." no encontrado."], 404);
        } else {
            $userDeleted = $userExist->delete();

            if (!$userDeleted) {
                return response()->json(["success" => false, "message" => "Error al eliminar vendedor."], 500);
            } else {
                return response()->json(["success" => true, "message" => "Vendedor eliminado exitosamente."], 200);
            }
        }
    }

}
