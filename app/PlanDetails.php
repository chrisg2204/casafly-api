<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PlanDetails extends Model
{

	protected $fillable = [
		'currency',
		'price',
		'expiration_time',
		'publications_for_sale',
		'publications_for_rent'
	];

	protected $guarded = [
		'id'
	];

	protected $table = 'plan_details';

	public $timestamps = false;

	public function TransacPlanDetail()
	{
		return $this->hasMany(TransacPlanDetail::class, 'plan_detail_id', 'id');
	}
	
}
