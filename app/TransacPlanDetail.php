<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TransacPlanDetail extends Model
{
	protected $fillable = [
		'plan_id',
		'plan_detail_id'
	];

	protected $guarded = [
		'id'
	];

	protected $table = 'transac_plan_details';

	public $timestamps = false;

	public function Plan()
	{
		return $this->belongsTo(Plan::class, 'plan_id', 'id');
	}

	public function PlanDetails()
	{
		return $this->belongsTo(PlanDetails::class, 'plan_detail_id', 'id');
	}

}
