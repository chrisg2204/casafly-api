<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('login', 'PassportController@login');
Route::post('register', 'PassportController@register');


Route::group(['middleware' => 'auth:api'], function(){
	Route::get('get-details', 'PassportController@getDetails');
	Route::delete('logout', 'PassportController@logout');
	Route::put('change-password', 'PassportController@changePassword');
});

/**
 * Plan.
 */

// Agregar plan.
Route::post('plans', 'PlanController@addPlans');
// Encontrar un plan por Id.
Route::get('plans/{id}', 'PlanController@findOnePlan')->where('id', '[0-9]+');
// Encontrar todos los planes.
Route::get('plans', 'PlanController@findPlans');
// Eliminar un plan.
Route::delete('plans/{id}', 'PlanController@deleteOnePlan')->where('id', '[0-9]+');
// Actualizar un plan.
Route::put('plans/{id}', 'PlanController@updatePlan')->where('id', '[0-9]+');

/**
 * Usuarios.
 */

// Encontrar todos los usuarios (vendedores).
Route::get('users', 'UserController@findUsers');
// Actializa usuario (vendedor) por Id.
Route::put('users/{id}', 'UserController@updateUser')->where('id', '[0-9]+');
// Elimina usuario (vendedor) por Id.
Route::delete("users/{id}", "UserController@deleteUser")->where('id', '[0-9]+');
